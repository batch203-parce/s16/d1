//console.log("Hello World");

// [SECTION] Javascript Arithmetic Operators

let x = 100;
let y = 25;

// Addition
let sum = x + y;
console.log("The result of addition operator: " +sum);

// Subtraction
let difference = x - y;
console.log("The result of subtraction operator: " +difference);

// Multiplication
let product = x * y;
console.log("The result of multiplication operator: " +product);

// Division
let quotent = x / y;
console.log("The result of division operator: " +quotent);

// Modulo
let modulo = x % y;
console.log("The result of modulo operator: " +modulo);


// [SECTION] Assignmnet Operator
// Basic Assignment Operator (=)
// The assignment operator assigns the value of the "right hand" operand to a variable.
let assignmentNumber = 8;
console.log("The current value of the assignmentNumber variable: " + assignmentNumber);

// Addition Assignment Operator --- 8+2
//long hand method assignmentNumber = assignmentNumber + 2; 
assignmentNumber += 2; // short hand method
console.log("Result of addition assignmentNumber operator: " +assignmentNumber);

assignmentNumber -= 2; // short hand method
console.log("Result of subtraction assignmentNumber operator: " +assignmentNumber);

assignmentNumber *= 2; // short hand method
console.log("Result of multiplication assignmentNumber operator: " +assignmentNumber);

assignmentNumber /= 2; // short hand method
console.log("Result of division assignmentNumber operator: " +assignmentNumber);

// [SECTION] PEMDAS (Order of Operators)
// Multiple Operators and Parethesis
// When multiple operators are applied in a single statement, it follows PEMDAS.
 let mdas = 1 + 2 - 3 * 4 / 5;
 console.log("The result of mdas operation: " +mdas);

 let pemdas = 1 + (2 - 3) * (4 / 5);
 console.log("The result of pemdas operation: " +pemdas);

// grouping the operators using parenthesis will deliver different result.
 let pemdass = (1 + (2 - 3)) * (4 / 5);
 console.log("The result of pemdas operation: " +pemdass);

 // [SECTION] Increment and Decrement
 // Operators that ass or subtract a valuesby 1 and reassign the value of the variable where the increment (++) / decrement (--) was applied.

 // Increment
 let z = 1;
 // The value of "z" is added by a value of one before returning the value and storing it in the variable.
 let increment = ++z; //1+1
 console.log("Result of pre-increment: " + increment);
 console.log("Result of pre-increment for z: " +z);
 // JavaScript read the code from top to bottom and ledt and right.
 // The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by 1
 increment = z++; 
 console.log("Result of post-increment: " + increment);
 console.log("Result of post-increment for z: " +z);

 // Decrement
 let decrement = --z;
 console.log("Result of pre-decrement: " +decrement);
 console.log("Result of pre-increment: " +z);

 decrement = z--;
 console.log("Result of post-decrement: " +decrement);
 console.log("Result of post-increment: " +z);

 // [SECTION] Type Coercion
 // Is the automatic conversion of values from one data type to another.

 let numA = '10';
 let numB = 12;

 // Coercion values usually performs concatenation if a string value is involved.
 let coercion = numA + numB;
 console.log(coercion);
 console.log(typeof coercion);

 let numC = 16;
 let numD = 14;

 let nonCoercion = numC + numD;
 console.log(nonCoercion);
 console.log(typeof nonCoercion);

 /*
	- The result is a number
	- The boolean  "true" is also associated with the number 1.
 */
 let numE = true + 1;
 console.log(numE);

  /*
	- The result is a number
	- The boolean  "false" is also associated with the number 0.
 */
 let numF = false + 1;
 console.log(numF);

 // [SECTION] Comparison Operators
 // Comparison operators are used to evaluate and comapre the left and right operands.
 // After evaluation, it returns a boolean value.

 let juan = "juan";

 // Equality Operator (==)
 /*
	- Checks whether the operands are equal/have the same content.
 	- Attempt to CONVERT ADN COMPARE operands of different data type.
 */

console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(1 == 'one'); //false
console.log(0 == false); //true
// Compare two strings that are the same.
console.log('juan' == 'juan'); //true
console.log('juan' == 'Juan'); //flase
// Compare a string with the variable juan
console.log('juan' == juan);

// Inequality Operator (!=)
/*
	- Checks whether the operands are not equal/have the different value.
	- Attempts to CONVERT AND COMPARE operand of different data type.
*/
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(0 != false); //false
console.log(1 != true); //false

// Strict Equality Operator (===)
// Checks whether the operands are equal/have the same content.
// Also COMPARES the DATA TYPES of 2 Values.
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //false
console.log(0 === false)//false
console.log(1 === true)//false
console.log('juan' === 'juan');//true
console.log('juan' === juan);//true

// Strict Inequality Operator (!==)
// Checks whether the operands are not equal/don'thave the same content.
// Also COMPARES the DATA TYPES of 2 Values.
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false)//true
console.log(1 !== true)//true
console.log('juan' !== 'juan');//false
console.log('juan' !== juan);//flase

// [SECTION] Relational Operators
// Some comparison operators check whether one values is greater or less than to the other value.

let a = 50;
let b = 65;

// GT or Greater Than Operator (>)
let isGreaterThan = a > b;
console.log(isGreaterThan); //false

// LT or Less Than Operator (<)
let isLessThan = a < b;
console.log(isLessThan); //true

// GTE or Greater Than or Equal (>=)
let isGTOrEqual = a >= b;
console.log(isGTOrEqual); //false

// LTE or Less Than or Equal (>=)
let isLTOrEqual = a <= b;
console.log(isLTOrEqual); //true

let numStr = "30";
console.log(a > numStr); //true - forced coercion to change the string to number.

let str = "twenty";
console.log(b >= str); //false
// Since the string is not numeric, the string was not converted to a number. 65 >= NaN (Not a Number);

// [SECTION] Logical Operators
// Logical Operators allows us to be more specific in the logical combination of conditions and evaluations. It returns boolean.

let isLegalAge = true;
let isRegistered = false;

// Logical AND operator (&& - Double Ampersand)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirementsMet);

// Logical OR Operator (|| Double Pipe)
// Returns true if one of the operand are true.
// Returns false if all operands are false.
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + someRequirementsMet);

// Logical NOT Operator (! - Exclamation Point);
// Returns the opposite value.
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT Operator: " + someRequirementsNotMet);
